from kbcstorage.client import Client

from kbc.env_handler import KBCEnvHandler
from datetime import datetime
import logging
import sys
import pytz

APP_VERSION = '0.0.3'
DT_FORMAT = '%Y-%m-%dT%H:%M:%S%z'

NEVERDATE = '1900-01-01T10:58:31+0100'


class TableChecker(KBCEnvHandler):
    def __init__(self, mandatory_params, data_path=None):
        KBCEnvHandler.__init__(self, mandatory_params, data_path=data_path)

    def run(self, debug=False):
        # override debug from config
        if(self.cfg_params.get('debug')):
            debug = True

        self.set_default_logger('DEBUG' if debug else 'INFO')
        logging.info('Running version %s', APP_VERSION)
        logging.info('Loading configuration...')
        self.validateConfig()

        params = self.cfg_params

        tables = params['table_list']
        token = params['#token']
        base_url = params.get('base_url','https://connection.keboola.com')
        last_state = self.get_state_file()

        last_run = None
        last_table_updates = {}
        curr_table_updates = {}

        now = datetime.now(pytz.utc)

        if last_state and last_state.get('last_run'):
            last_run = datetime.strptime(last_state.get('last_run'), DT_FORMAT)
            last_table_updates = last_state.get('table_updates')

        client = Client(base_url, token)

        not_changed = True
        not_changed_tables = []
        logging.info('Checking tables %s', str(tables))
        for table in tables:
            t_detail = client.tables.detail(table)

            last_change = datetime.strptime(NEVERDATE if not t_detail.get(
                'lastChangeDate') else t_detail.get('lastChangeDate'), DT_FORMAT)
           #last_import = datetime.strptime(t_detail.get('lastImportDate'), DT_FORMAT)
            curr_table_updates[t_detail['id']
                               ] = last_change.strftime(DT_FORMAT)

            if last_run and last_table_updates:
                not_changed = last_change <= datetime.strptime(
                    last_table_updates.get(table, NEVERDATE), DT_FORMAT)
                if not_changed:
                    not_changed_tables.append(t_detail['id'])

        state = {'last_run': now.strftime(DT_FORMAT),
                 'table_updates': curr_table_updates}
        self.write_state_file(state)

        if len(not_changed_tables) == 0:
            logging.info('All tables %s were changed', str(tables))
        else:
            logging.error(
                'Some tables have not changed since last run: %s', str(not_changed_tables))
            sys.exit(1)


"""
        Main entrypoint
"""
if __name__ == "__main__":
    comp = TableChecker(['table_list', '#token'])
    comp.run()
