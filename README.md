Checks table status based on `lastChangeDate`. If all tables has changed since last run, passes OK, if not, fails with error.

## Config:
```json
{
  "table_list": ["out.c-test.stst","out.c-test.test" ],
  "#token": "STORAGE_TOKEN",
  "base_url": "https://connection.keboola.com"
}
```